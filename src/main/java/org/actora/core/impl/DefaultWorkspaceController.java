package org.actora.core.impl;

import org.actora.core.WorkspaceController;
import org.actora.core.profile.Profile;
import org.actora.core.workspace.Workspace;
import org.actora.core.workspace.WorkspaceControllerException;
import org.actora.core.workspace.WorkspaceKey;

import java.util.List;
import java.util.Optional;

public class DefaultWorkspaceController implements WorkspaceController {

    @Override
    public Workspace createWorkspace(WorkspaceKey workspaceKey, Profile profile) {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public List<Workspace> listWorkspaces() throws WorkspaceControllerException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public Optional<Workspace> getByKey(WorkspaceKey workspaceKey) throws WorkspaceControllerException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

}
