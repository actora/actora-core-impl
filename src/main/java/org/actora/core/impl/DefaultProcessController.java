package org.actora.core.impl;

import org.actora.core.ProcessController;
import org.actora.core.ProcessFactory;
import org.actora.core.process.PID;
import org.actora.core.process.Process;
import org.actora.core.process.ProcessControllerException;
import org.actora.core.process.ProcessDetails;

import java.util.List;
import java.util.Optional;

public class DefaultProcessController implements ProcessController {

    @Override
    public Process startProcess(ProcessDetails processDetails) throws ProcessControllerException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public void setProcessFactory(ProcessFactory processFactory) {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public List<Process> listRunningProcesses() throws ProcessControllerException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public Optional<PID> getProcess(PID pid) throws ProcessControllerException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

}
